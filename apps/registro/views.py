# -*- coding: utf-8 -*-
from __future__ import unicode_literals



from django.shortcuts import render,redirect
from django.urls import reverse_lazy

from django.http import HttpResponse,HttpResponseRedirect

from django.views.generic import ListView, UpdateView, CreateView, DeleteView
	
from apps.registro.forms import atletaForm,atletaFormWithPay

from apps.registro.models import atleta
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

from django.core.mail import send_mail

import hashlib 


def conteo():
	return(atleta.objects.filter(carrera_atleta="Media Maraton",pago_confirmado=True).count(),atleta.objects.filter(carrera_atleta="10k").count()+100,atleta.objects.filter(carrera_atleta="Media Maraton").count(),atleta.objects.filter(carrera_atleta="Silla De Ruedas").count())

def enviar_correo(correo,nombre,carrera,categoria):
	categoria_atleta=categoria.upper()
	fecha_carrera='ABRIL 7 2019'
	nombre_atleta=nombre.upper()
	competencia_atleta=carrera.upper()
	text='<div style="background-image: url'+"('https://mediavillavicencio.herokuapp.com/static/images/banner2.jpg')"+';background-repeat: no-repeat;background-attachment: absolute; background-position:center;position:relative;top:0;left:0;right:0">'+'<div style="background-color:rgba(0,0,0,0.7);font family'+":'Verdana';margin:10%;padding:10%;color:"+'white">'+'<h1 style="text-align: center"> Hola '+nombre_atleta+'</h1>'+" "+'<h2 style="text-align:center;color: #f2f2f2">'+'TU INSCRIPCIÓN A LA <span style="color:#428bca">'+competencia_atleta+'</span> A CELEBRARSE EN LA CIUDAD DE VILLAVICENCIO EL <span style="color:#5cb85c">'+ fecha_carrera +'</span> EN LA CATEGORÍA <span style="color:#5cb85c">'+categoria_atleta+'</span> FUE <span style="color:#d9534f"> COMPLETADA EXITOSAMETE </span> </h2><h1><img src="http://www.villavicencio.gov.co/Style%20Library/images/logo-alcaldia.png"></h1></div></div>'
	send_mail(
    'Inscripción Completada '+competencia_atleta+' Ciudad De Villavicencio',
    '',
    'mediamaratonvillavicencio@gmail.com',
    [correo],
    fail_silently=False,
    html_message=text,
	)

def enviar_correo2(correo,nombre,carrera,categoria):
	categoria_atleta=categoria.upper()
	fecha_carrera='ABRIL 7 2019'
	nombre_atleta=nombre.upper()
	competencia_atleta=carrera.upper()
	text='Nuevo Corredor: '+nombre_atleta+' Carrera: '+competencia_atleta+' Categoria: '+categoria_atleta
	send_mail(
    'Inscripción Completada---> '+nombre_atleta,
    '',
    'mediamaratonvillavicencio@gmail.com',
    [correo],
    fail_silently=False,
    html_message=text,
	)



def createAtleta(req):
	if req.method=='POST':
			correo=req.POST.get('correo_atleta')
			cedula=req.POST.get('identificacion_atleta')
			nombre=req.POST.get("nombre_atleta")+' '+req.POST.get("apellido_atleta")
			categoria=req.POST.get('categoria_atleta')
			carrera=req.POST.get('carrera_atleta')
			if(atleta.objects.filter(correo_atleta=correo).exists()):
				return JsonResponse({'alerta':'Correo Registrado previamente'})
			else:	
				valor=40000
				api="o6y7ENS2dxTgm9a5I78SwTz8OZ"
				merchan="789496"
				referencia=cedula
				currency="COP"
				string_signature=""+api+"~"+merchan+"~"+referencia+"~"+str(valor)+"~COP"
				code = hashlib.md5()
				code.update(string_signature.encode())
				firma=code.hexdigest()
				form =atletaForm(req.POST)
				if form.is_valid():
					form.save()
				if (carrera=='10k' or carrera=='Silla De Ruedas'):
					enviar_correo(correo,nombre,'carrera '+carrera,categoria)
					enviar_correo2("mediamaratonvillavicencio@gmail.com",'Nuevo Corredor'+nombre,'carrera '+carrera,categoria)
					return render(req,"inscripcion_libre.html",{'nombre':nombre})
				else:		
					return render(req,"pago.html",{'categoria':categoria,'correo':correo,'nombre':nombre,'signature':firma,'ref':referencia,'valor':valor,'cedula':req.POST.get('identificacion_atleta')})
	else:
		form=atletaForm()
	
	return render(req,"registro.html",{'form':form})	


class atleta_delete(DeleteView):
	model=atleta
	template_name="atleta_delete.html"
	success_url= reverse_lazy('registro:listado_atletas')




def atletasLista(req):
		
		if (req.user.username=='Wilder_Herrera'):
			if not ('order_name' in req.session):
				req.session['order_name']=False

			
			if(req.session['order_name']):
					object_list=atleta.objects.all().order_by('-nombre_atleta')
					req.session['order_name']=not(req.session['order_name'])	
			else:
					object_list=atleta.objects.all().order_by('nombre_atleta')
					req.session['order_name']=not(req.session['order_name'])	
			check_pay,t10k,tmm,tsr=conteo()
			
			return(render(req,"listado_atletas.html",{'object_list':object_list,'pago':check_pay,'total_10k':t10k,'total_mm':tmm,'total_sr':tsr}))

		elif (req.user.username=='IMDER'):
			check_pay,t10k,tmm,tsr=conteo()
			return(render(req,"listado_atletas.html",{'total_10k':t10k,'total_mm':tmm,'total_sr':tsr}))
		else:
			return JsonResponse({'user':'no available'})


def order_by_carrera(req):

	if (req.user.username=='Wilder_Herrera'):
		if not ('order_carrera' in req.session):
			req.session['order_carrera']=False
		
		if(req.session['order_carrera']):
				object_list=atleta.objects.all().order_by('-carrera_atleta')

				req.session['order_carrera']=not(req.session['order_carrera'])	

		else:
				object_list=atleta.objects.all().order_by('carrera_atleta')

				req.session['order_carrera']=not(req.session['order_carrera'])	

		check_pay,t10k,tmm,tsr=conteo()
		return(render(req,"listado_atletas.html",{'object_list':object_list,'pago':check_pay,'total_10k':t10k,'total_mm':tmm,'total_sr':tsr}))
	else:
		return JsonResponse({'user':'no available'})

def order_by_pay(req):
	
	if (req.user.username=='Wilder_Herrera'):	
		if not ('order_pay' in req.session):
			req.session['order_pay']=False

		
		if(req.session['order_pay']):
				object_list=atleta.objects.all().order_by('pago_confirmado')

				req.session['order_pay']=not(req.session['order_pay'])	

		else:
				object_list=atleta.objects.all().order_by('-pago_confirmado')

				req.session['order_pay']=not(req.session['order_pay'])		


		check_pay,t10k,tmm,tsr=conteo()
		return(render(req,"listado_atletas.html",{'object_list':object_list,'pago':check_pay,'total_10k':t10k,'total_mm':tmm,'total_sr':tsr}))
	else:
		return JsonResponse({'user':'no available'})


@csrf_exempt 
def confirmacion_pago(req):
	if(int(req.POST.get("state_pol")) ==4 and req.POST.get("response_message_pol")=="APPROVED") :	
		atleta_query=atleta.objects.filter(identificacion_atleta=req.POST.get("extra1"))
		atleta_query.update(pago_confirmado=True)
		email=(req.POST.get('email_buyer'))
		nombre=(atleta_query.values('nombre_atleta')[0]['nombre_atleta'])
		categoria=(req.POST.get('extra2'))##Categoria
		carrera=atleta_query.values('carrera_atleta')[0]['carrera_atleta']
		enviar_correo(email,nombre,carrera,categoria) ###PONER DATOS FUNCION EMAIL
		print ("Pago Confirmado")


	return (HttpResponse(status=200))



def check_form(req):
	correo=req.GET.get("correo")
	cedula=req.GET.get("cedula")
	identificacion_msg=""
	correo_msg=""


	if atleta.objects.filter(correo_atleta=correo).exists():
		correo_msg="El correo electronico ya fue registrado"
	elif len(correo)==0:
		 correo_msg="Ingrese un correo electronico"
	else:
		correo_msg="undefined"

	if atleta.objects.filter(identificacion_atleta=cedula).exists() :
		identificacion_msg="La identificacion ya fue registrada"

	elif len(cedula)==0:
		 identificacion_msg="Ingrese un numero de identificacion"
	else: 
		identificacion_msg="undefined"

	return JsonResponse({"cedula":identificacion_msg,"correo":correo_msg})

class atleta_edit(UpdateView):
	model=atleta
	template_name="atleta_edit.html"
	form_class=atletaFormWithPay
	success_url= reverse_lazy('registro:listado_atletas')


class atletaDelete(DeleteView):
    model = atleta
    success_url = reverse_lazy('registro:listado_atletas')


def pago_atrasado(req):
	if (req.method=='GET'):
		return render(req,'pago_atrasado.html')

	else:
		print("aqui")
		cedula=req.POST.get('cedula')
		atleta_query=atleta.objects.filter(identificacion_atleta=cedula)
		nombre=(atleta_query.values('nombre_atleta')[0]['nombre_atleta'])
		carrera=atleta_query.values('carrera_atleta')[0]['carrera_atleta']
		correo=atleta_query.values('correo_atleta')[0]['correo_atleta']
		categoria=atleta_query.values('categoria_atleta')[0]['categoria_atleta']
		valor=40000
		api="o6y7ENS2dxTgm9a5I78SwTz8OZ"
		merchan="789496"
		referencia=cedula
		currency="COP"
		string_signature=""+api+"~"+merchan+"~"+referencia+"~"+str(valor)+"~COP"
		code = hashlib.md5()
		code.update(string_signature.encode())
		firma=code.hexdigest()
		return render(req,"pago.html",{'categoria':categoria,'correo':correo,'nombre':nombre,'signature':firma,'ref':referencia,'valor':valor,'cedula':cedula})

@csrf_exempt 
def validacion(req):
		cedula=req.POST.get('cedula')
		if (atleta.objects.filter(carrera_atleta="Media Maraton",identificacion_atleta=cedula).exists()):
			return JsonResponse({'validacion':'existe'})
		else:
			return JsonResponse({'validacion':'No registrado'})


