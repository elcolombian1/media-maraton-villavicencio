from __future__ import unicode_literals

from django.db import models
import django.utils
import datetime

class atleta(models.Model):
	now = datetime.datetime.now()
	atleta_id = models.AutoField(primary_key=True)
	nombre_atleta=models.CharField(max_length=50)
	apellido_atleta=models.CharField(max_length=50)
	correo_atleta=models.EmailField()
	identificacion_atleta=models.CharField(max_length=50)
	edad_atleta=models.CharField(max_length=50)
	tipo_sangre_atleta=models.CharField(max_length=50)
	sexo_atleta=models.CharField(max_length=30)
	telefono_atleta=models.CharField(max_length=20)

	carrera_atleta=models.CharField(max_length=50,default="10k") ##PUEDE ELEGIR 10K O MEDIA MARATON 
	categoria_atleta=models.CharField(max_length=50,default=" ") ##PUEDE ELEGIR 10K O MEDIA MARATON 

	direccion_atleta=models.CharField(max_length=50)
	nacimiento_atleta=models.DateField(default= django.utils.timezone.now)
	talla_atleta=models.CharField(max_length=5,default="L")
	nombre_emergencia_atleta=models.CharField(max_length=30)
	telefono_emergencia_atleta=models.CharField(max_length=20)
	pago_confirmado=models.BooleanField(default=False)

	fecha_inscripcion =models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return '{} {}'.format(self.nombre_atleta,self.apellido_atleta)
	class Meta():
		ordering = ['-pago_confirmado']
	