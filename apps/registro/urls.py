from django.urls import path
from . import views
from django.contrib.auth.decorators import login_required

urlpatterns=[
	path(r'',views.createAtleta,name="registro"),
	path(r'listado/',login_required(views.atletasLista),name="listado_atletas"),
	path(r'listado/order_by_carrera/',login_required(views.order_by_carrera),name="listado_atletas_carerra"),
	path(r'listado/order_by_pay',login_required(views.order_by_pay),name="listado_atletas_pay"),
	path(r'cheking/',views.confirmacion_pago,name="cheking"),
	path(r'cheking_form/',views.check_form,name="check_form"),
	path(r'editar_atletas/(?P<pk>\d+)/$',views.atleta_edit.as_view(),name='editar_atleta'),
	path(r'borrar_atleta/(?P<pk>\d+)/$',views.atleta_delete.as_view(),name='delete'),
	path(r'pago_ticket/',views.pago_atrasado,name='pago_atrasado'),
	path(r'pago_ticket_validation/',views.validacion,name='validacion'),
	
	# path(r'^media_maraton/',views.registro_atleta_media,name="registro_atleta_media"),
	# path(r'^consulta/',views.consultar_atleta,name="consultar_atleta"),
	
]

