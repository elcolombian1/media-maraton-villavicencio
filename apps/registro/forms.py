from django import forms
from apps.registro.models import atleta

from django.contrib.admin.widgets import AdminDateWidget

class atletaForm(forms.ModelForm):
	carrera_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('','',),('Media Maraton', 'Media Maraton',),('10k', '10k',),('Silla De Ruedas', 'Silla De Ruedas',)))
	sexo_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('Masculino', 'Masculino',), ('Femenino', 'Femenino',)))
	tipo_sangre_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('A-','A+',),('B-','B+',),('O+', 'O+',), ('O-', 'O-',),('AB-','AB+',)))
	talla_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('XS', 'XS',),('S', 'S',), ('M', 'M',),('L', 'L',),('XL', 'XL',)))
	
	class Meta:

		model=atleta
		


		fields=[
				'nombre_atleta',
				'apellido_atleta',
				'correo_atleta',
				'identificacion_atleta',
				'edad_atleta',
				'tipo_sangre_atleta',
				'sexo_atleta',
				'telefono_atleta',
				'carrera_atleta',
				'direccion_atleta',
				##'nacimiento_atleta',
				'talla_atleta',
				'nombre_emergencia_atleta',
				'telefono_emergencia_atleta',
				'categoria_atleta',
				
				
		]
		labels={
				'nombre_atleta':'Nombre',
				'apellido_atleta':'Apellido',
				'correo_atleta':'Correo Electronico',
				'identificacion_atleta':'Identificación',
				'edad_atleta':'Edad',
				'tipo_sangre_atleta':'Tipo De Sangre (RH)',
				'sexo_atleta':'Genero',
				'telefono_atleta:':'Numero Telefonico',
				'carrera_atleta':'Competencia',
				'direccion_atleta':'Direccion Residencial',
				##'nacimiento_atleta':'Fecha De Nacimiento',
				'talla_atleta':'Talla De Camisa',
				'nombre_emergencia_atleta':'En caso de emergencia llamar a : ',
				'telefono_emergencia_atleta':'Numero de tu contacto de emergencia',
				
				

		}
		widgets={
				
				'nombre_atleta': forms.TextInput(attrs={'class':'form-control'}),
				'apellido_atleta':forms.TextInput(attrs={'class':'form-control'}),
				'correo_atleta':forms.EmailInput(attrs={'class':'form-control'}),
				'identificacion_atleta':forms.TextInput	(attrs={'class':'form-control'}),
				'edad_atleta':forms.NumberInput(attrs={'class':'form-control'}),
				'telefono_atleta':forms.TextInput(attrs={'class':'form-control'}),
				'direccion_atleta':forms.TextInput(attrs={'class':'form-control'}),
				'nombre_emergencia_atleta':forms.TextInput(attrs={'class':'form-control'}),
				'telefono_emergencia_atleta':forms.TextInput(attrs={'class':'form-control'}),
				
								
		}

class atletaFormWithPay(forms.ModelForm):
	talla_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('XS', 'XS',),('S', 'S',), ('M', 'M',),('L', 'L',),('XL', 'XL',)))
	categoria_atleta=forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), choices=(('10k', '10k',), ('Media Maraton', 'Media Maraton',)))
	class Meta:

		model=atleta
		


		fields=[
				'nombre_atleta',
				'apellido_atleta',
				'edad_atleta',
				'correo_atleta',
				'identificacion_atleta',
				'categoria_atleta',
				'talla_atleta',
				
				
		]
		labels={

		      	'nombre_atleta': 'Nombre',
				'apellido_atleta':'Apellido',
				'edad_atleta':'Edad',
				'correo_atleta':'Correo Electronico',
				'identificacion_atleta':'Identificacion',
				'categoria_atleta':'Categoria',
				'talla_atleta':'Talla',
				
				

		}
		widgets={
				
				'nombre_atleta': forms.TextInput(attrs={'class':'form-control'}),
				'apellido_atleta':forms.TextInput(attrs={'class':'form-control'}),
				'edad_atleta':forms.NumberInput(attrs={'class':'form-control'}),
				'correo_atleta':forms.EmailInput(attrs={'class':'form-control'}),
				'identificacion_atleta':forms.TextInput	(attrs={'class':'form-control'}),
				

				
		}