# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django.views.generic import CreateView
from django.urls import reverse_lazy



from django.shortcuts import render,redirect

from django.http import HttpResponse

from django.http import JsonResponse

from . import forms

class login(CreateView):
	model=User
	template_name="login.html"
	form_class=forms.login_register
	success_url=reverse_lazy("registro:listado_atletas")

